# 说明

本示例演示如何使用 [Apache Shiro](http://shiro.apache.org/ "Apache Shiro") 实现用户的登录、注销以及对功能的访问控制。

#### 运行环境:

- Java SE 1.8
- Apache Tomcat
- Apache Shiro
- HSQL DB

#### 开发工具:
- NetBeans 8.0

## 1. 使用Git工具从GitLab上下载源码到本地

```
git clone https://gitlab.com/panyun/sample-shiro.git
```

## 2. 源码目录结构
```
sample-shiro
|- data (HSQLDB数据库目录，将其复制到与Tomcat安装目录相同的磁盘根目录下。)
|- sample-shiro (程序源码)
   |- src (程序源码目录)
      |- main
         |- java
            |- org
               |- panyun
                  |- sample
                     |-shiro
                       |- controller (保存MVC模式中的控制器)
                          |- PrinterServlet.java
                       |- model (保存MVC模式中的模型)
                          |- Printer.java
         |- resources
         |- webapp (网站前端文件)
            |- css (CSS文件)
               |- bootstrap.min.css
               |- bootstrap-theme.min.css
            |- js (JavaScript文件)
               |- bootstrap.min.js
               |- jquery.min.js
            |- META-INF
               |- context.xml
            |- sites (网站HTML文件)
               |- authorization-fail.html (用户登录失败时的显示页面)
               |- index.html (用户登录成功后的显示页面)
               |- printer-manage.html (管理打印机时的显示页面)
               |- printer-print.html (打印文档时的显示页面)
            |- WEB-INF
               |- shiro.ini (Shiro的配置文件)
               |- web.xml (Web应用配置文件)
            |- index.html (默认首页)
            |- login.html (登录页面)
   |- nb-configuration.xml (NetBeans运行环境配置文件)
   |- pom.xml (Maven配置文件)
```

## 3. 数据库

### 3.1 Users表
|字段|类型|说明|主键|
|---|---|---|---|
|userId|char(36)|用户ID|是|
|username|varchar(16)|登录名|
|password|varchar(256)|密码|
|realname|varchar(16)|姓名|
|status|char(10)|状态|

### 3.2 Roles表
|字段|类型|说明|主键|
|---|---|---|---|
|roleId|char(36)|角色ID|是|
|roleName|varchar(32)|角色名||
|description|varchr(128)|描述||
|status|char(10)|状态|

### 3.3 Permissions表
|字段|类型|说明|主键|
|---|---|---|---|
|permissionId|char(36)|权限ID|是|
|permissionName|varchar(32)|权限名||
|description|varchr(128)|描述||
|status|char(10)|状态|

### 3.4 Users_Roles表
|字段|类型|说明|主键|外键|
|---|---|---|---|---|
|userId|char(36)|用户ID|是|是, Users(userId)|
|roleId|char(36)|角色ID|是|是, Roles(roleId)|

### 3.5 Roles_Permissions表
|字段|类型|说明|主键|外键|
|---|---|---|---|---|
|roleId|char(36)|角色ID|是|是, Roles(roleId)|
|permissionId|char(36)|权限ID|是|是，Permissions(permissionId)|

## 4. 测试用数据
|用户名|密码|角色|权限|
|---|---|---|---|
|admin|admin|Administrator|printer:*|
|john|john|Default|printer:print|
|tom|tom|Printer Admin|printer:manage|
