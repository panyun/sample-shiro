package org.panyun.sample.shiro.controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.panyun.sample.shiro.model.Printer;

/**
 *
 * @author panyun
 */
@WebServlet(name = "PrinterServlet", urlPatterns = {"/printerServlet"})
public class PrinterServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(PrinterServlet.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Subject currentUser = SecurityUtils.getSubject();
        String action = request.getParameter("action");
        Printer printer = new Printer();
        switch (action) {
            case "print":
                if (currentUser.isPermitted("printer:print")) {
                    printer.print();
                    response.sendRedirect("sites/printer-print.html");
                } else {
                    response.sendRedirect("sites/authorization-fail.html");
                    LOG.log(Level.WARNING, "{0} tried to perform unauthorized actions.", currentUser.getPrincipal().toString());
                }
                break;
            case "manage":
                if (currentUser.isPermitted("printer:manage")) {
                    printer.manage();
                    response.sendRedirect("sites/printer-manage.html");
                } else {
                    response.sendRedirect("sites/authorization-fail.html");
                    LOG.log(Level.WARNING, "{0} tried to perform unauthorized actions.", currentUser.getPrincipal().toString());
                }
                break;
            default:
                LOG.warning("Invalid printer action.");
                response.sendRedirect("sites/index.html");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
