package org.panyun.sample.shiro.model;

/**
 *
 * @author panyun
 */
public class Printer {

    public void print() {
        System.out.println("You are printing document.");
    }

    public void manage() {
        System.out.println("You are managing your printer.");
    }
}
